package week_3

class Rational(x: Int, y: Int) {
  require(y != 0, "denominator must be nonzero!")
	
  def this(x: Int) = this(x, 1)
	
  private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
  private val g = gcd(x, y)
		
  val numer = x / g
  val denom = y / g
	
  def < (that: Rational) = this.numer * that.denom < this.denom * that.numer
  def max(that: Rational) = if (this < that) that else this
  def + (that: Rational) =
    new Rational(that.numer * denom + that.denom * numer, that.denom * denom)
			
  override def toString = numer + "/" + denom
	
  def unary_- : Rational = new Rational(-numer, denom)
	
  def - (that: Rational) = this + -that
	
  def * (that: Rational) = new Rational(that.numer * numer, that.denom * denom)
}
