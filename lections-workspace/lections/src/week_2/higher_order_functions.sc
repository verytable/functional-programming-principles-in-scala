package week_2

object higher_order_functions {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def sumInts(a: Int, b: Int): Int =
    if (a > b) 0 else a + sumInts(a + 1, b)       //> sumInts: (a: Int, b: Int)Int

  def cube(x: Int) = x * x * x                    //> cube: (x: Int)Int

  def sumCubes(a: Int, b: Int): Int =
    if (a > b) 0 else cube(a) + sumCubes(a + 1, b)//> sumCubes: (a: Int, b: Int)Int
    
  def sum(f: Int => Int, a: Int, b: Int): Int =
    if (a > b) 0
    else f(a) + sum(f, a + 1, b)                  //> sum: (f: Int => Int, a: Int, b: Int)Int

  def sumInts2(a: Int, b: Int) = sum(id, a, b)    //> sumInts2: (a: Int, b: Int)Int
  def sumCubes2(a: Int, b: Int) = sum(cube, a, b) //> sumCubes2: (a: Int, b: Int)Int
  def id(x: Int): Int = x                         //> id: (x: Int)Int

  def sumInts3(a: Int, b: Int) = sum(x => x, a, b)//> sumInts3: (a: Int, b: Int)Int

  def sumCubes3(a: Int, b: Int) = sum(x => x * x * x, a, b)
                                                  //> sumCubes3: (a: Int, b: Int)Int
  
  //tail recursive sum
  def sumTail(f: Int => Int, a: Int, b: Int): Int = {
    def loop(a: Int, acc: Int): Int = {
      if (a > b) acc
      else loop(a + 1, f(a) + acc)
    }
    loop(a, 0)
  }                                               //> sumTail: (f: Int => Int, a: Int, b: Int)Int
  
  sumTail(x => x * x, 3, 5)                       //> res0: Int = 50
}