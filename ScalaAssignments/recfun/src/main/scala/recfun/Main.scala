package recfun
import common._

object Main {
  def main(args: Array[String]) {
    println("Pascal's Triangle")
    for (row <- 0 to 10) {
      for (col <- 0 to row)
        print(pascal(col, row) + " ")
      println()
    }
    println(countChange(4, List(2, 1)))
  }

  /**
   * Exercise 1
   */
  def pascal(c: Int, r: Int): Int = {
    if (c == 0 || c == r) 1 else pascal(c - 1, r - 1) + pascal(c, r - 1)
  }

  /**
   * Exercise 2
   */
  def balance(chars: List[Char]): Boolean = {

    def bracketsDiff(chars: List[Char], curBalance: Int): Boolean = {
      if (curBalance < 0) 
        false
      else if (chars.isEmpty)
        curBalance == 0
      else if (chars(0) == '(')
        bracketsDiff(chars.tail, curBalance + 1)
      else if (chars(0) == ')')
        bracketsDiff(chars.tail, curBalance - 1)
      else
        bracketsDiff(chars.tail, curBalance)
    }

    bracketsDiff(chars, 0)
  }

  /**
   * Exercise 3
   */
  def countChange(money: Int, coins: List[Int]): Int = {
    if (coins.isEmpty)
      0
    else if (coins(0) == money)
      1 + countChange(money, coins.tail)
    else if (coins(0) > money)
      countChange(money, coins.tail)
    else
      countChange(money, coins.tail) + countChange(money - coins(0), coins)
  }
}
