package week_2

object currying {

  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int =
      if (a > b) 0
      else f(a) + sumF(a + 1, b)
    sumF
  }                                               //> sum: (f: Int => Int)(Int, Int) => Int
  
  def sumInts = sum(x => x)                       //> sumInts: => (Int, Int) => Int
  
  sumInts(3, 5)                                   //> res0: Int = 12
  sum(x => x)(1, 10)                              //> res1: Int = 55
  
  def sumCubes = sum(x => x * x * x)              //> sumCubes: => (Int, Int) => Int
  
  sumCubes(3, 4)                                  //> res2: Int = 91
  
  //second parameters group can be ommited
  def sum2(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 0 else f(a) + sum2(f)(a + 1, b)    //> sum2: (f: Int => Int)(a: Int, b: Int)Int
  sum2(x => x)(1, 10)                             //> res3: Int = 55
  
  //all parameters are essential
  def sum3(f: Int => Int, a: Int, b: Int): Int =
    if (a > b) 0 else f(a) + sum3(f, a + 1, b)    //> sum3: (f: Int => Int, a: Int, b: Int)Int
  sum3(x => x, 1, 10)                             //> res4: Int = 55
  
  def product(f: Int => Int)(a: Int, b: Int): Int =
    if (a > b) 1 else f(a) * product(f)(a + 1, b) //> product: (f: Int => Int)(a: Int, b: Int)Int
    
  product(x => x * x)(3, 4)                       //> res5: Int = 144
  
  def fact(n: Int) = product(x => x)(1, n)        //> fact: (n: Int)Int
  
  fact(5)                                         //> res6: Int = 120
  
  def mapReduce(f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b: Int): Int =
    if (a > b) zero
    else combine(f(a), mapReduce(f, combine, zero)(a + 1, b))
                                                  //> mapReduce: (f: Int => Int, combine: (Int, Int) => Int, zero: Int)(a: Int, b
                                                  //| : Int)Int
  def product2(f: Int => Int)(a: Int, b: Int): Int =
    mapReduce(f, (x, y) => x * y, 1)(a, b)        //> product2: (f: Int => Int)(a: Int, b: Int)Int
  product2(x => x)(3, 4)                          //> res7: Int = 12
}