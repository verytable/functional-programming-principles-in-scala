//package week_3

import week_3.Rational          //imports just rational
import week_3._                 //imports everything defined in week_3
import week_3.{Rational, Hello} //imports both Rational and Hello

object scratch {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  new Rational(1, 2)                              //> res0: week_3.Rational = 1/2
  def error(msg: String) = throw new Error(msg)   //> error: (msg: String)Nothing
  //error("test")
  
  //every reference class type also has null as a value
  //the type of null is Null
  //Null is a subtype of every class that inherits from Object
  val x = null                                    //> x  : Null = null
  val y: String = null                            //> y  : String = null
  
  //type of this expression is a supertype of Int and Boolean
  if (true) 1 else false                          //> res1: AnyVal = 1
}