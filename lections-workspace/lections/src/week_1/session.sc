package week_1

object session {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet

  def abs(x: Double) = if (x < 0) -x else x       //> abs: (x: Double)Double

  def sqrt(x: Double) = {

    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improve(guess))

    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.001

    def improve(guess: Double) =
      (guess + x / guess) / 2

    sqrtIter(1.0)
  }                                               //> sqrt: (x: Double)Double

  sqrt(2)                                         //> res0: Double = 1.4142156862745097
  sqrt(1e-6)                                      //> res1: Double = 0.0010000001533016628
  sqrt(1e60)                                      //> res2: Double = 1.0000788456669446E30

  def sum(xs: List[Int]): Int =
    if (xs.isEmpty) 0 else xs(0) + sum(xs.tail)   //> sum: (xs: List[Int])Int

  def max(xs: List[Int]): Int = {

    def maxAuxiliary(xs: List[Int], hasElement: Boolean): Int =
      if (hasElement)
        if (xs.isEmpty) 0
        else if (maxAuxiliary(xs.tail, true) > xs(0)) maxAuxiliary(xs.tail, true)
        else xs(0)
      else throw new java.util.NoSuchElementException

    maxAuxiliary(xs, !xs.isEmpty)
  }                                               //> max: (xs: List[Int])Int

  max(List(3))                                    //> res3: Int = 3
  
  def gcd(a: Int, b: Int): Int =
		if (b == 0) a else gcd(b, a % b)  //> gcd: (a: Int, b: Int)Int

  gcd(14, 21)                                     //> res4: Int = 7

  def factorial(n: Int): Int =
    if (n == 0) 1 else n * factorial(n - 1)       //> factorial: (n: Int)Int

  factorial(4)                                    //> res5: Int = 24
  
  def tailrecFactorial(n: Int): Int = {
  	def loop(acc: Int, n: Int): Int =
  		if (n == 0) acc
  		else loop(acc * n, n -1)
  	loop(1 , n)
  }                                               //> tailrecFactorial: (n: Int)Int
  
  tailrecFactorial(16)                            //> res6: Int = 2004189184
}