package week_5

object higherOrderLists {
  val nums = List(2, -4, 5 , 1, 5 ,2)             //> nums  : List[Int] = List(2, -4, 5, 1, 5, 2)
  val fruits = List("apple", "orange", "pineapple", "banana")
                                                  //> fruits  : List[String] = List(apple, orange, pineapple, banana)
  nums filter (x => x > 0)                        //> res0: List[Int] = List(2, 5, 1, 5, 2)
  nums filterNot (x => x > 0)                     //> res1: List[Int] = List(-4)
  nums partition (x => x > 0)                     //> res2: (List[Int], List[Int]) = (List(2, 5, 1, 5, 2),List(-4))
 
  //longest prefix satisgying p
  nums takeWhile (x => x > 0)                     //> res3: List[Int] = List(2)
  //list except prefix satisfying p
  nums dropWhile (x => x > 0)                     //> res4: List[Int] = List(-4, 5, 1, 5, 2)
  //combination of takeWhile and dropWhile
  nums span (x => x > 0)                          //> res5: (List[Int], List[Int]) = (List(2),List(-4, 5, 1, 5, 2))
 
  val data = List("a", "a", "a", "b", "c", "c")   //> data  : List[String] = List(a, a, a, b, c, c)
 
  def pack[T](xs: List[T]): List[List[T]] = xs match {
    case Nil => Nil
    case x :: xs1 =>
      val (first, rest) = xs span (y => y == x)
      first :: pack(rest)
  }                                               //> pack: [T](xs: List[T])List[List[T]]
 
  pack(data)                                      //> res6: List[List[String]] = List(List(a, a, a), List(b), List(c, c))
 
  def encode[T](xs: List[T]): List[(T, Int)] =
    pack(xs) map (ys => (ys.head, ys.length))     //> encode: [T](xs: List[T])List[(T, Int)]
  encode(data)                                    //> res7: List[(String, Int)] = List((a,3), (b,1), (c,2))
  
  def concat[T](xs: List[T], ys: List[T]): List[T] =
    (xs foldRight ys) (_ :: _)                    //> concat: [T](xs: List[T], ys: List[T])List[T]
  	
  concat(nums, fruits)                            //> res8: List[Any] = List(2, -4, 5, 1, 5, 2, apple, orange, pineapple, banana)
}