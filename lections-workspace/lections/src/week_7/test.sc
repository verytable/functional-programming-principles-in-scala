package week_7

object test {
	val problem = new Pouring(Vector(4, 9, 19))
                                                  //> problem  : week_7.Pouring = week_7.Pouring@48a158
	problem.moves                             //> res0: scala.collection.immutable.IndexedSeq[Product with Serializable with we
                                                  //| ek_7.test.problem.Move] = Vector(Empty(0), Empty(1), Empty(2), Fill(0), Fill(
                                                  //| 1), Fill(2), Pour(0,1), Pour(0,2), Pour(1,0), Pour(1,2), Pour(2,0), Pour(2,1)
                                                  //| )
 
  problem.pathSets.take(2).toList                 //> res1: List[Set[week_7.test.problem.Path]] = List(Set(-->Vector(0, 0, 0)), Se
                                                  //| t(Fill(0)-->Vector(4, 0, 0), Fill(1)-->Vector(0, 9, 0), Fill(2)-->Vector(0, 
                                                  //| 0, 19)))
                                                  
	problem.solutions(16)                     //> res2: Stream[week_7.test.problem.Path] = Stream(Fill(2) Pour(2,1) Empty(1) P
                                                  //| our(2,1) Pour(2,0) Fill(2) Pour(2,0)-->Vector(4, 9, 16), ?)
}