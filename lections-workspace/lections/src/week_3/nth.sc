import week_3._

object nth {
  //function selects n'th element of the list
  def nth[T](n: Int, xs: List[T]): T =
    if (xs.isEmpty) throw new IndexOutOfBoundsException
    else if (n == 0) xs.head
    else nth(n - 1, xs.tail)                      //> nth: [T](n: Int, xs: week_3.List[T])T

  val list = new Cons(1, new Cons(2, new Cons(3, Nil)))
                                                  //> list  : week_3.Cons[Int] = week_3.Cons@1a245b0
  nth(2, list)                                    //> res0: Int = 3
}