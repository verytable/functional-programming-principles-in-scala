package week_6

object maps {

	val romanNumerals = Map('I' -> 1, 'V' -> 5, 'X' -> 10)
                                                  //> romanNumerals  : scala.collection.immutable.Map[Char,Int] = Map(I -> 1, V -> 
                                                  //| 5, X -> 10)
	val capitalOfCountry = Map("US" -> "Washington", "Switzerland" -> "Bern")
                                                  //> capitalOfCountry  : scala.collection.immutable.Map[String,String] = Map(US -
                                                  //| > Washington, Switzerland -> Bern)
	capitalOfCountry get "US"                 //> res0: Option[String] = Some(Washington)
	capitalOfCountry get "Andorra"            //> res1: Option[String] = None
	
	def showCapital(country: String) = capitalOfCountry.get(country) match {
		case None => "missing data"
		case Some(capital) => capital
	}                                         //> showCapital: (country: String)String
	
	showCapital("Switzerland")                //> res2: String = Bern
	showCapital("Germany")                    //> res3: String = missing data
	
	val cap1 = capitalOfCountry withDefaultValue("unknown")
                                                  //> cap1  : scala.collection.immutable.Map[String,String] = Map(US -> Washington
                                                  //| , Switzerland -> Bern)
	cap1("Andorra")                           //> res4: String = unknown
	
	val fruit = List("apple", "pineapple", "orange", "pear")
                                                  //> fruit  : List[String] = List(apple, pineapple, orange, pear)
	fruit sortWith(_.length < _.length)       //> res5: List[String] = List(pear, apple, orange, pineapple)
	fruit.sorted                              //> res6: List[String] = List(apple, orange, pear, pineapple)
	
	fruit groupBy (_.head)                    //> res7: scala.collection.immutable.Map[Char,List[String]] = Map(p -> List(pine
                                                  //| apple, pear), a -> List(apple), o -> List(orange))
}