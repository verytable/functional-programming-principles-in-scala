object polynomials {
  
  class Poly(terms0: Map[Int, Double]) {
  	def this(bindings: (Int, Double)*) = this(bindings.toMap)
  	val terms = terms0 withDefaultValue 0.0
  	/*def + (other: Poly) = new Poly(terms ++ (other.terms map adjust))
  	def adjust(term: (Int, Double)): (Int, Double) = {
  		val (exp, coeff) = term
  		exp -> (coeff + terms(exp))
  	}*/
  	
  	//more efficient way than one in comments above
  	def + (other: Poly) = new Poly((other.terms foldLeft terms)(addTerm))
  	def addTerm(terms: Map[Int, Double], term: (Int, Double)): Map[Int, Double]= {
  		val (exp, coeff) = term
  		terms + (exp -> (coeff + terms(exp)))
  	}
  	
  	override def toString =
  		(for ((exp, coeff) <- terms.toList.sorted.reverse) yield coeff + " x^" + exp) mkString " + "
  }
  
  val p1 = new Poly(Map(1 -> 2.0, 3 -> 4.0, 5 -> 6.2))
                                                  //> p1  : polynomials.Poly = 6.2 x^5 + 4.0 x^3 + 2.0 x^1
  val p2 = new Poly(Map(0 -> 3.0, 3 -> 7.0))      //> p2  : polynomials.Poly = 7.0 x^3 + 3.0 x^0
  p1 + p2                                         //> res0: polynomials.Poly = 6.2 x^5 + 11.0 x^3 + 2.0 x^1 + 3.0 x^0
  p1.terms(7)                                     //> res1: Double = 0.0
  
  val p3 = new Poly(0 -> 1.9, 1 -> 2.0, 4 -> 5.4) //> p3  : polynomials.Poly = 5.4 x^4 + 2.0 x^1 + 1.9 x^0
}