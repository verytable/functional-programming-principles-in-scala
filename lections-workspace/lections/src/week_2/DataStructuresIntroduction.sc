package week_2

object DataStructuresIntroduction {
  val x = new Rational(1, 3)                      //> x  : week_2.Rational = 1/3
  x.numer                                         //> res0: Int = 1
  x.denom                                         //> res1: Int = 3
  val y = new Rational(5, 7)                      //> y  : week_2.Rational = 5/7
  x + y                                           //> res2: week_2.Rational = 22/21
  val z = new Rational(3, 2)                      //> z  : week_2.Rational = 3/2
  x - y - z                                       //> res3: week_2.Rational = -79/42
  y + y                                           //> res4: week_2.Rational = 10/7
  x < y                                           //> res5: Boolean = true
  x.max(y)                                        //> res6: week_2.Rational = 5/7
  x max y                                         //> res7: week_2.Rational = 5/7
  //val strange = new Rational(1, 0)
  new Rational(2)                                 //> res8: week_2.Rational = 2/1
}

class Rational(x: Int, y: Int) {
    require(y != 0, "denominator must be nonzero!")
    
    def this(x: Int) = this(x, 1)
    
    private def gcd(a: Int, b: Int): Int = if (b == 0) a else gcd(b, a % b)
    private val g = gcd(x, y)
    
    val numer = x / g
    val denom = y / g
    
    def < (that: Rational) = this.numer * that.denom < this.denom * that.numer
    
    def max(that: Rational) = if (this < that) that else this
    
    def + (that: Rational) =
      new Rational(
        that.numer * denom + that.denom * numer,
        that.denom * denom)
        
    override def toString = numer + "/" + denom
    
    def unary_- : Rational = new Rational(-numer, denom)
    
    def - (that: Rational) = this + -that
    
    def * (that: Rational) =
      new Rational(that.numer * numer, that.denom * denom)
}