package week_6

object test {
	
	val xs = Array(1, 2, 3, 4 ,5)             //> xs  : Array[Int] = Array(1, 2, 3, 4, 5)
	xs map (x => 2 * x)                       //> res0: Array[Int] = Array(2, 4, 6, 8, 10)
	
	val s = "Hello, World!"                   //> s  : String = Hello, World!
	s filter (c => c.isUpper)                 //> res1: String = HW
	s exists (c => c.isUpper)                 //> res2: Boolean = true
	s forall (c => c.isUpper)                 //> res3: Boolean = false
	
	val pairs = List(1, 2, 3) zip s           //> pairs  : List[(Int, Char)] = List((1,H), (2,e), (3,l))
	pairs.unzip                               //> res4: (List[Int], List[Char]) = (List(1, 2, 3),List(H, e, l))
	
	s flatMap (c => List('.', c))             //> res5: String = .H.e.l.l.o.,. .W.o.r.l.d.!
	s map (c => List('.', c))                 //> res6: scala.collection.immutable.IndexedSeq[List[Char]] = Vector(List(., H),
                                                  //|  List(., e), List(., l), List(., l), List(., o), List(., ,), List(.,  ), Lis
                                                  //| t(., W), List(., o), List(., r), List(., l), List(., d), List(., !))
  xs.sum                                          //> res7: Int = 15
  xs.max                                          //> res8: Int = 5
  
  (1 to 3) flatMap (x => (1 to 2) map (y => (x, y)))
                                                  //> res9: scala.collection.immutable.IndexedSeq[(Int, Int)] = Vector((1,1), (1,2
                                                  //| ), (2,1), (2,2), (3,1), (3,2))
  
	def scalarProduct(xs: Vector[Double], ys: Vector[Double]): Double =
		(xs zip ys).map(xy => xy._1 * xy._2).sum
                                                  //> scalarProduct: (xs: Vector[Double], ys: Vector[Double])Double
  
  def scalarProduct2(xs: Vector[Double], ys: Vector[Double]): Double =
  	(xs zip ys).map{case (x, y) => x * y}.sum //> scalarProduct2: (xs: Vector[Double], ys: Vector[Double])Double
  	
  	
	def isPrime(n: Int): Boolean = (2 until n) forall (d => n % d != 0)
                                                  //> isPrime: (n: Int)Boolean
                                                  
	isPrime(97)                               //> res10: Boolean = true
	isPrime(51)                               //> res11: Boolean = false
}