package funsets

import org.scalatest.FunSuite
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import scala.runtime.TraitSetter

/**
 * This class is a test suite for the methods in object FunSets. To run
 * the test suite, you can either:
 *  - run the "test" command in the SBT console
 *  - right-click the file in eclipse and chose "Run As" - "JUnit Test"
 */
@RunWith(classOf[JUnitRunner])
class FunSetSuite extends FunSuite {


  /**
   * Link to the scaladoc - very clear and detailed tutorial of FunSuite
   *
   * http://doc.scalatest.org/1.9.1/index.html#org.scalatest.FunSuite
   *
   * Operators
   *  - test
   *  - ignore
   *  - pending
   */

  /**
   * Tests are written using the "test" operator and the "assert" method.
   */
  test("string take") {
    val message = "hello, world"
    assert(message.take(5) == "hello")
  }

  /**
   * For ScalaTest tests, there exists a special equality operator "===" that
   * can be used inside "assert". If the assertion fails, the two values will
   * be printed in the error message. Otherwise, when using "==", the test
   * error message will only say "assertion failed", without showing the values.
   *
   * Try it out! Change the values so that the assertion fails, and look at the
   * error message.
   */
  test("adding ints") {
    assert(1 + 2 === 3)
  }
  
  import FunSets._

  test("contains is implemented") {
    assert(contains(x => true, 100))
  }
  
  /**
   * When writing tests, one would often like to re-use certain values for multiple
   * tests. For instance, we would like to create an Int-set and have multiple test
   * about it.
   * 
   * Instead of copy-pasting the code for creating the set into every test, we can
   * store it in the test class using a val:
   * 
   *   val s1 = singletonSet(1)
   * 
   * However, what happens if the method "singletonSet" has a bug and crashes? Then
   * the test methods are not even executed, because creating an instance of the
   * test class fails!
   * 
   * Therefore, we put the shared values into a separate trait (traits are like
   * abstract classes), and create an instance inside each test method.
   * 
   */

  trait TestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(1)
  }

  /**
   * This test is currently disabled (by using "ignore") because the method
   * "singletonSet" is not yet implemented and the test would fail.
   * 
   * Once you finish your implementation of "singletonSet", exchange the
   * function "ignore" by "test".
   */
  test("singletonSet(1) contains 1") {
    
    /**
     * We create a new instance of the "TestSets" trait, this gives us access
     * to the values "s1" to "s3". 
     */
    new TestSets {
      /**
       * The string argument of "assert" is a message that is printed in case
       * the test fails. This helps identifying which assertion failed.
       */
      assert(contains(s1, 1), "Singleton1")
      assert(contains(s2, 2), "Singleton2")
      assert(!contains(s3, 1), "Singleton3")
    }
  }

  test("union contains all elements") {
    new TestSets {
      val s = union(s1, s2)
      val t = union(s1, s4)
      assert(contains(s, 1), "Union 1")
      assert(contains(s, 2), "Union 2")
      assert(!contains(s, 3), "Union 3")
      assert(contains(t, 1), "Union 4")
      assert(!contains(t,2), "Union 5")
    }
  }
  
  trait BigTestSets {
    val s1 = singletonSet(1)
    val s2 = singletonSet(2)
    val s3 = singletonSet(3)
    val s4 = singletonSet(4)
    val s12 = union(s1, s2)
    val s13 = union(s1, s3)
    val s123 = union(s12, s3)
    val s1234 = union(s123, s4)
  }
  
  test("intersection contains each element") {
    new BigTestSets {
      val s = intersect(s12, s13)
      val t = intersect(s1, s2)
      assert(contains(s, 1), "Intersection 1")
      assert(!contains(s, 2), "Intersection 2")
      assert(!contains(t, 1), "Intersections 3")
      assert(!contains(t, 2), "Intersections 3")
      assert(!contains(t, 3), "Intersections 3")
    }
  }
  
  test("diff") {
    new BigTestSets {
      val s = diff(s123, s12)
      val t = diff(s12, s123)
      val v = diff(s1234, s1234)
      assert(contains(s, 3), "Difference 1")
      assert(!contains(t, 1), "Difference 2")
      assert(!contains(t, 3), "Difference 3")
      assert(!contains(v, 1), "Difference 4")
      assert(!contains(v, 2), "Difference 5")
      assert(!contains(v, 3), "Difference 6")
      assert(!contains(v, 4), "Difference 7")
    }
  }
  
  test("forall and exists") {
    new BigTestSets {
      assert(forall(s1, x => true), "Forall 1")
      assert(forall(s1234, x => true), "Forall 2")
      assert(!forall(s1234, x => x % 2 == 0), "Forall 3")
      assert(exists(s1, x => true), "Exists 1")
      assert(exists(s1234, x => x % 2 == 0), "Exists 2")
    }
  }
  
  test("map") {
    new BigTestSets {
      val s = map(s1234, x => x)
      val t = map(s1234, x => x * x)
      assert(contains(s, 1), "Map 1")
      assert(contains(s, 2), "Map 2")
      assert(contains(t, 1), "Map 3")
      assert(contains(t, 4), "Map 4")
      assert(contains(t, 16), "Map 5")
      assert(!contains(t, 3), "Map 6")
    }
  }
  
  test("filter") {
    new BigTestSets {
     val s = filter(s1234, x => x == 1)
     val t = filter(s1234, x => x % 2 == 0)
     assert(contains(s, 1), "Filter 1")
     assert(!contains(s, 2), "Filter 2")
     assert(contains(t, 2), "Filter 3")
     assert(contains(t, 4), "Filter 4")
     assert(!contains(t, 1), "Filter 5")
     assert(!contains(t, 3), "Filter 6")
    }
  }
}
