package week_4

//Peano numbers
abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  def successor: Nat = new Successor(this)
  def +(that: Nat): Nat
  def -(that: Nat): Nat
}

object Zero extends Nat {
  def isZero = true
  def predecessor = throw new Error("0.predecessor")
  def +(that: Nat): Nat = that
  def -(that: Nat): Nat = if (that.isZero) Zero else throw new Error("negative number")
}

class Successor(n: Nat) extends Nat {
  def isZero = false
  def predecessor = n
  def +(that: Nat): Nat = new Successor(n + that)
  def -(that: Nat): Nat = if (that.isZero) this else n - that.predecessor
}